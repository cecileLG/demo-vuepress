---
permalink: /mutations/stack/
---

# Stack 

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

## Front

Je déteste le petit miroir de l’escalier. Il ne reflète que nos têtes, il nous décapite, et ma bouche est trop grande, mes yeux sont trop rapprochés. Quand je ris je montre trop mes gencives. Mon visage est éclipsé par le visage farouche de Suzanne, par ses yeux que les poètes aimeront, disait Bernard, à voir se baisser sur un ouvrage de couture aux points égaux et blancs. Même le visage lunaire et vide de Rhoda se suffit à lui-même, comme ces pétales blancs qu’elle aimait à faire flotter dans un bol. 

## Back

C’est pourquoi je les dépasse toutes deux d’un seul bond, pour ne m’arrêter qu’à l’étage au-dessus, où pend un long miroir dans lequel je peux me refléter tout entière. Je vois d’un seul coup d’oeil mon corps et mon visage, car en dépit de cette robe de serge, mon corps et mon visage ne font qu’un. Oui, lorsque je remet la tête, mon corps étroit ondule tout entier; et mes jambes minces frémissent comme une tige au souffle du vent. Je voltige entre le dur visage de Suzanne, et celui de Rhoda, si vague; je bondis comme une de ces flammes qui courent dans les crevasses du sol; je remue; je danse; je ne cesse jamais de remuer et de danser. je remue comme la feuille qui remuait jadis dans la baie, et m’effrayait quand j’étais enfant. Ma danse se projette sur ces murs impersonnels, ces murs rayés, ces murs blanchis à la chaux et bordés d’une plinthe jaune, comme la lueur du feu sur le couvercle d’une théière. Je prends feu, même sous les yeux froids des femmes. 

## Visualisation

Quand je lis, un liseré rouge court le long de la tranche noire des livres de classe. Et pourtant, je suis incapable de suivre un mot à travers ses significations changeantes. Je suis incapable de suivre une pensée qui remonte du présent au passé. Je ne me tiens pas debout comme Suzanne, tout absorbée, songeant à ma maison avec des yeux pleins de larmes; je ne vais pas m’étendre comme Rhoda, pelotonnée sur moi-même au milieu des fougères qui tachent de vert mon tablier de coton rose, en rêvant de plantes sous-marines et de roches entre lesquels nagent lentement des poissons. Je ne rêve pas.

