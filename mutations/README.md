---
permalink: /mutations/
---

# Introduction

Tout ce que vous voulez savoir sur le cas d'usage Mutations sans jamais l'avoir demandé !

Ce dépôt contient: 
- la [problématique metier de développement du CU](0-problematique.md), 
- la [stack technique](1-stack.md),
- les [données](2-donnees.md) utilisées et les traitements réalisés.


<CurrentGroupToc></CurrentGroupToc>