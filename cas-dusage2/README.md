---
permalink: /cas-dusage2/
---

# Introduction

::: tip General
Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
:::

::: Exemple boite d'information
At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi.
:::


## Lien 1

Voyez l’extraordinaire assurance de Jinny lorsqu’elle enfile ses bas. Pourtant, elle va simplement jouer au tennis. Voilà ce que j’admire chez elle.

## Lien 2

::: tip Conseil de lecture
Nous vous recommandons de lire Virginia Woolf **en intégralité**.
:::