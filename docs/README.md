---
home: true
heroImage: /hero.png
heroText: Documentation CU
tagline: Toute la doc, rien que de la doc 
actionText: Consulter la doc →
actionLink: /accueil
features:
- title: Normes de developpement
  details: Minimal setup with markdown-centered project structure helps you focus on writing.
- title: Cas d'usage
  details: Enjoy the dev experience of Vue + webpack, use Vue components in markdown, and develop custom themes with Vue.
- title: Une equipe de choc
  details: VuePress generates pre-rendered static HTML for each page, and runs as an SPA once a page is loaded.
footer: MIT Licensed | Copyright © 2018-present Evan You
---